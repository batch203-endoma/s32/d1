let http = require("http")

const port = 4000;

//Mock Database

let directory = [{
    name: "Brandon",
    email: "brandon@mail.com"
  },
  {
    name: "Jobert",
    email: "jobert@mail.com"
  }
]
const server = http.createServer((request, response) => {
  //When the `/users` route is access with a mehthod of "GET" we will send the directory (mock database)list of users

  if (request.url == "/users" && request.method == "GET") {
    response.writeHead(200, {
      "Content-Type": "application/json"
    });
    response.write(JSON.stringify(directory));
    response.end();
  }
  //we want to received the content of the request and save it to the mock database.
  // content will be retrieve from ther request body
  // Parse the received JSON request body to Javascript object.
  //add the parse object to the directory (mock database)
  if (request.url == "/users" && request.method == "POST") {
    //this will act as a placeholder for the resources/data to be created later on.
    let requestBody = "";

    //data-step
    request.on("data", (data) => {
      console.log(data);

      //assigns the data retrieved from the data to stream to request
      requestBody += data;
      console.log(requestBody);
    })

    // response end step -only runs after the request has completely sent.
    request.on("end", ()=>{
      console.log(typeof requestBody);

      requestBody = JSON.parse(requestBody);

      console.log(typeof requestBody);

      directory.push(requestBody);

      console.log(directory);

      response.writeHead(200, {"Content-Type": "application/json"});
      response.write(JSON.stringify(requestBody));
      response.end();
    });
  }
});

server.listen(port);
console.log(`server running at localhost: ${port}`);
